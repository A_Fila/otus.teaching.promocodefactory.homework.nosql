﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public interface ITableService<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync();
    }
}
