﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        //private readonly IRepository<Preference> _preferencesRepository;
        private readonly ITableService<Preference> _prefTable;

        public PreferencesController(ITableService<Preference> prefTable)//IRepository<Preference> preferencesRepository
        {
            //_preferencesRepository = preferencesRepository;
            _prefTable = prefTable;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            //var preferences = await _preferencesRepository.GetAllAsync();

            //var response = preferences.Select(x => new PreferenceResponse()
            //{
            //    Id = x.Id,
            //    Name = x.Name
            //}).ToList();
            var prefs = await _prefTable.GetAllAsync();

            return Ok(prefs);//response
        }
    }
}