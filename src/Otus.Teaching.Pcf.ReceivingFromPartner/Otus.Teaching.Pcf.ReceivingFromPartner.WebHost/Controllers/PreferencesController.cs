﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
      : ControllerBase
    {
        //private readonly IRepository<Preference> _preferencesRepository;
        private readonly ITableService<Preference> _prefTable;

        public PreferencesController(//IRepository<Preference> preferencesRepository)
                                     ITableService<Preference> prefTable)
        {
            //_preferencesRepository = preferencesRepository;
            _prefTable = prefTable;
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            //var preferences = await _preferencesRepository.GetAllAsync();

            //var response = preferences.Select(x => new PreferenceResponse()
            //{
            //    Id = x.Id,
            //    Name = x.Name
            //}).ToList();
             var prefs = await _prefTable.GetAllAsync();

            return Ok(prefs); //response
        }
    }

}