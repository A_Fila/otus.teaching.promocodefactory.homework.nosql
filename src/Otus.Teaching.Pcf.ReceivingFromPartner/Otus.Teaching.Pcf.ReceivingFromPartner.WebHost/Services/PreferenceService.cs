﻿//using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class PreferenceService<T> : ITableService<T> where T :class
    {
        private readonly HttpClient _httpClient;

        public PreferenceService(HttpClient httpClient) 
        {
            _httpClient = httpClient;
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            //var res = await _httpClient.GetStringAsync("api/v1/Preferences");

            var prefs = await _httpClient.GetFromJsonAsync<IEnumerable<T>>("api/v1/Preferences");

            return prefs;
        }   
    }
}