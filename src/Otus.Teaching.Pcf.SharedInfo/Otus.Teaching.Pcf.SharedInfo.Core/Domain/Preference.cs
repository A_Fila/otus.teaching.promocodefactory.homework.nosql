﻿using System;

namespace Otus.Teaching.Pcf.SharedInfo.Core.Domain
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
    }
}