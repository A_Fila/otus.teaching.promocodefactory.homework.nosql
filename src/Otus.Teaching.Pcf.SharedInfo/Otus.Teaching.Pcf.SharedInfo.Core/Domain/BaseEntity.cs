﻿using System;

namespace Otus.Teaching.Pcf.SharedInfo.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}