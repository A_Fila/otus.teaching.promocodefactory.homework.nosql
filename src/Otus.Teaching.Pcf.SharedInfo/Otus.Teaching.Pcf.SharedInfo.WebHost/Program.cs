using Microsoft.EntityFrameworkCore;

using Otus.Teaching.Pcf.SharedInfo.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.SharedInfo.DataAccess;
using Otus.Teaching.Pcf.SharedInfo.DataAccess.Data;
using Otus.Teaching.Pcf.SharedInfo.DataAccess.Repositories;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
string strConn = builder.Configuration.GetConnectionString("PromocodeFactorySharedInfoDb");
//Console.WriteLine($"ORM connectionString:{strConn}");

//postgres
builder.Services.AddDbContext<DataContext>(
                     option => option.UseNpgsql(strConn)
                     .LogTo(Console.WriteLine, LogLevel.Information)
                     .EnableSensitiveDataLogging()
                );
//Redis distributed cache
var redisConn = builder.Configuration.GetConnectionString("Redis");
//Console.WriteLine($"Redis connectionString:{redisConn}");
builder.Services.AddStackExchangeRedisCache(options => options.Configuration = redisConn);

//builder.Services.AddSingleton<IConnectionMultiplexer>( opt => 
//                            ConnectionMultiplexer.Connect(strConn));

builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.Logger.LogInformation($"ORM ConnectionString:{strConn}");
app.Logger.LogInformation($"REDIS ConnectionString:{redisConn}");


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) //|| true
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

//init DB
//resolve a scoped service for a limited duration when the app starts
using (var serviceScope = app.Services.CreateScope())
{
    var services = serviceScope.ServiceProvider;
    var dbInitializer = services.GetRequiredService<IDbInitializer>();

    //Use the service
    dbInitializer.InitializeDb();
}
//var dbInitializer = app.Services.GetService<IDbInitializer>();
//dbInitializer.InitializeDb();


app.Run();