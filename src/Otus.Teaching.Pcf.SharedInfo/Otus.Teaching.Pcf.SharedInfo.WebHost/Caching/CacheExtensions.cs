﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Otus.Teaching.Pcf.SharedInfo.WebHost.Caching
{
    public static class CacheExtensions
    {
        public static string ToJsonString(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static async Task<T> GetCacheValueAsync<T>(this IDistributedCache cache, string key) where T : class
        {
            string? result = await cache.GetStringAsync(key);

            if (String.IsNullOrEmpty(result))
                return null;
            
            var deserializedObj = JsonConvert.DeserializeObject<T>(result);

            return deserializedObj;
        }

        public static async Task SetCacheValueAsync<T>(this IDistributedCache cache, string key, T value, TimeSpan expiration) where T : class
        {
            DistributedCacheEntryOptions cacheEntryOptions = new DistributedCacheEntryOptions
            {
                // Remove item from cache after duration
                AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(60),

                // Remove item from cache if unused for the duration
                SlidingExpiration = expiration// TimeSpan.FromSeconds(30);
            };

            string result = value.ToJsonString();

            await cache.SetStringAsync(key, result, cacheEntryOptions);
        }
    }
}