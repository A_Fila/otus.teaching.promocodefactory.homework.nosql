﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.SharedInfo.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.SharedInfo.Core.Domain;
using Otus.Teaching.Pcf.SharedInfo.WebHost.Models;
using Otus.Teaching.Pcf.SharedInfo.WebHost.Caching;

namespace Otus.Teaching.Pcf.SharedInfo.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly ILogger<PreferencesController> _logger;
        private readonly IDistributedCache _cache;

        public PreferencesController(IRepository<Preference> preferencesRepository,
                                     ILogger<PreferencesController> _logger,
                                     IDistributedCache cache)
        {
            _preferencesRepository = preferencesRepository;
            _cache = cache;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            const string CACHE_KEY = "preferences";
            const long CACHE_EXPIRATION_IN_SEC = 60;

            // Check if content exists in cache
            var storedPrefs = await  _cache.GetCacheValueAsync< List<PreferenceResponse>>(CACHE_KEY);
                        
            if (storedPrefs != null)
                return storedPrefs;

            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            await _cache.SetCacheValueAsync(CACHE_KEY, response, TimeSpan.FromSeconds(CACHE_EXPIRATION_IN_SEC) );

            return Ok(response);
        }
    }
}