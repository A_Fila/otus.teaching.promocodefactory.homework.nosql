﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.SharedInfo.Core.Domain;
using System;

namespace Otus.Teaching.Pcf.SharedInfo.DataAccess
{
    public class DataContext
        : DbContext
    {
        //static DataContext()
        //{
        //    //fix: Cannot write DateTime with Kind=Unspecified to PostgreSQL type 'timestamp with time zone', only UTC is supported.
        //    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        //}


        public DbSet<Preference> Preferences { get; set; }

        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}