﻿namespace Otus.Teaching.Pcf.SharedInfo.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}